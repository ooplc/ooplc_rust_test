#![no_main]
#![no_std]
#![feature(type_alias_impl_trait)]

use ooplc_test as _; // global logger + panicking-behavior + memory layout

// TODO(7) Configure the `rtic::app` macro
#[rtic::app(
    // TODO: Replace `some_hal::pac` with the path to the PAC
    device = stm32f4xx_hal::pac,
    // TODO: Replace the `FreeInterrupt1, ...` with free interrupt vectors if software tasks are used
    // You can usually find the names of the interrupt vectors in the some_hal::pac::interrupt enum.
    dispatchers = [SDIO,]
)]
mod app {
    use stm32f4xx_hal::prelude::{_fugit_RateExtU32, _fugit_ExtU32};
    use stm32f4xx_hal::rcc::RccExt;
    use stm32f4xx_hal::syscfg::SysCfgExt;
    use stm32f4xx_hal::gpio::{GpioExt, ExtiPin, Edge, Pin, Output, PinState};
    use stm32f4xx_hal::pac::{EXTI, I2C1, I2C3, UART4, SPI1};
    use stm32f4xx_hal::i2c::{self, I2c};
    use stm32f4xx_hal::syscfg::SysCfg;
    use stm32f4xx_hal::otg_fs::{USB, UsbBus, UsbBusType};
    use stm32f4xx_hal::rtc::Rtc;
    use stm32f4xx_hal::timer::pwm::PwmExt;
    use stm32f4xx_hal::serial::{SerialExt, Tx};
    use stm32f4xx_hal::time::U32Ext;
    use stm32f4xx_hal::spi::{self, SpiExt, Spi};
    use rtic_monotonics::systick::Systick;
    use stm32f4xx_hal::timer::{Channel1, Channel2, Channel3};
    use usb_device::LangID;
    use usb_device::device::{UsbDeviceBuilder, UsbVidPid, StringDescriptors, UsbDevice};
    use usb_device::bus::UsbBusAllocator;
    use core::fmt::Write;
    // needed for setting the datetime in the rtc
    //use time::{PrimitiveDateTime, Date, Time, Month};


    const BMP280_ADDRESS: u8 = 0x77;
    const BME280_ADDRESS: u8 = 0x18;
    static mut EP_MEMORY: [u32; 1024] = [0; 1024];
    // there may be better options than a static Option, but this works for now
    static mut USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None;
    

    // Shared resources go here
    #[shared]
    struct Shared {
        usb_serial: usbd_serial::SerialPort<'static, UsbBusType>,
    }

    // Local resources go here
    #[local]
    struct Local {
        valve1_out: Pin<'B', 2, Output>,
        valve3_out: Pin<'B', 1, Output>,
        valve4_out: Pin<'B', 0, Output>,
        fan_out: Pin<'C', 5, Output>,
        shutdown_in: Pin<'C', 10>,
        valve1_in: Pin<'C', 11>,
        valve3_in: Pin<'D', 2>,
        valve4_in: Pin<'B', 5>,
        i2c_bus_1: I2c<I2C1>,
        i2c_bus_3: I2c<I2C3>,
        usb_dev: UsbDevice<'static, UsbBusType>,
        rtc: Rtc,
        fram_cs: Pin<'A', 4, Output>,
        fram_spi: Spi<SPI1>,
    }

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local) {
        defmt::info!("init");

        // HSE based clocks
        let clocks = cx.device.RCC.constrain()
            .cfgr
            .use_hse(25.MHz())
            .sysclk(180.MHz())
            .require_pll48clk()
            .freeze();
        let token = rtic_monotonics::create_systick_token!();
        Systick::start(cx.core.SYST, 180_000_000, token);

        // RTC based on LSE
        let mut rtc = Rtc::new(cx.device.RTC, &mut cx.device.PWR);
        //rtc.set_datetime(&PrimitiveDateTime::new(Date::from_calendar_date(2023, Month::December, 6).unwrap(), Time::from_hms(15, 5, 10).unwrap()));
        let datetime = rtc.get_datetime();
        defmt::info!("date: {}-{}-{} {}:{}:{}", datetime.year(), datetime.month() as u8, datetime.day(), datetime.hour(), datetime.minute(), datetime.second());

        // grab io ports
        let gpioa = cx.device.GPIOA.split();
        let gpiob = cx.device.GPIOB.split();
        let gpioc = cx.device.GPIOC.split();
        let gpiod = cx.device.GPIOD.split();
        let gpioe = cx.device.GPIOE.split();

        // setup outputs
        let valve1_out = gpiob.pb2.into_push_pull_output_in_state(PinState::High);
        let valve3_out = gpiob.pb1.into_push_pull_output_in_state(PinState::High);
        let valve4_out = gpiob.pb0.into_push_pull_output_in_state(PinState::High);
        let fan_out = gpioc.pc5.into_push_pull_output_in_state(PinState::High);
        let _siroflow_out = gpioc.pc4.into_push_pull_output_in_state(PinState::High);
        let _blower_out = gpioa.pa3.into_push_pull_output_in_state(PinState::High);
        let _auxillary_out = gpioa.pa2.into_push_pull_output_in_state(PinState::High);

        // pwm outputs
        let led_red = Channel3::new(gpioe.pe13).with_complementary(gpiob.pb15);
        let led_green = Channel2::new(gpioe.pe11).with_complementary(gpiob.pb14);
        let led_blue = Channel1::new(gpioe.pe9).with_complementary(gpiob.pb13);
        let mut pwm_channels = cx.device.TIM1.pwm_hz((led_blue, led_green, led_red), 20.kHz(), &clocks).split();
        let (mut blue_channel, ref mut green_channel, ref mut red_channel) = pwm_channels;
        let max_duty_cycle = red_channel.get_max_duty();
        red_channel.set_duty(max_duty_cycle/6);
        red_channel.enable_complementary();
        green_channel.set_duty(max_duty_cycle/20);
        green_channel.enable_complementary();
        blue_channel.set_duty(max_duty_cycle/2);
        blue_channel.enable_complementary();


        // prepare for exti interrupts
        let mut syscfg = cx.device.SYSCFG.constrain();
        let mut exti = cx.device.EXTI;

        // setup inputs
        let shutdown_in = setup_interrupt_pin(gpioc.pc10, &mut syscfg, &mut exti);
        let valve1_in = setup_interrupt_pin(gpioc.pc11, &mut syscfg, &mut exti);
        let valve3_in = setup_interrupt_pin(gpiod.pd2, &mut syscfg, &mut exti);
        let valve4_in = setup_interrupt_pin(gpiob.pb5, &mut syscfg, &mut exti);


        // i2c 
        let i2c1 = cx.device.I2C1;
        let i2c2 = cx.device.I2C2;
        let i2c3 = cx.device.I2C3;
        let mut i2c_bus_1 = I2c::new(i2c1, (gpiob.pb6, gpiob.pb7), i2c::Mode::Standard { frequency: 400.kHz()}, &clocks);
        let mut _i2c_bus_2 = I2c::new(i2c2, (gpiob.pb10, gpioc.pc12), i2c::Mode::Standard { frequency: 400.kHz()}, &clocks);
        let i2c_bus_3 = I2c::new(i2c3, (gpioa.pa8, gpioc.pc9), i2c::Mode::Standard { frequency: 400.kHz()}, &clocks);

        match i2c_bus_1.write(BMP280_ADDRESS, &[0xF4, 0xB7]) {
            Ok(_) => {}
            Err(error) => i2c_error(1, error),
        }

        i2c_read::spawn().ok();

        // usb serial
        let usb = USB::new(
            (cx.device.OTG_FS_GLOBAL, cx.device.OTG_FS_DEVICE, cx.device.OTG_FS_PWRCLK),
            (gpioa.pa11, gpioa.pa12),
            &clocks
        );
        unsafe {
            USB_BUS.replace(UsbBus::new(usb, &mut EP_MEMORY));
        }
        let usb_serial = usbd_serial::SerialPort::new(unsafe {USB_BUS.as_ref().unwrap()});
        // vid and pid are completely random
        let usb_dev = UsbDeviceBuilder::new(unsafe{USB_BUS.as_ref().unwrap()}, UsbVidPid(0xAE20, 0xCE16))
            .strings(&[StringDescriptors::new(LangID::EN)
                .manufacturer("WoToGo")
                .product("ooPLC")
                .serial_number("TEST")
            ]).unwrap()
            .device_class(usbd_serial::USB_CLASS_CDC)
            .build();

        
        // uart serial
        let tx_pin = gpioa.pa0;
        let _rx_pin = gpioa.pa1;
        let mut tx: Tx<UART4, u8> = cx.device.UART4.tx(tx_pin, 9600.bps(), &clocks).unwrap();
        writeln!(tx, "test uart tx").unwrap();

        // fram via spi
        let mut fram_cs = gpioa.pa4.into_push_pull_output_in_state(PinState::High);
        let spi_mode = spi::Mode{
            polarity: spi::Polarity::IdleLow,
            phase: spi::Phase::CaptureOnFirstTransition,
        };
        let mut fram_spi = cx.device.SPI1.spi((gpioa.pa5, gpioa.pa6, gpioa.pa7), spi_mode, 25.MHz(), &clocks);

        spi_check::spawn().ok();

        (
            Shared {
                usb_serial,
            },
            Local {
                valve1_out,
                valve3_out,
                valve4_out,
                fan_out,
                shutdown_in,
                valve1_in,
                valve3_in,
                valve4_in,
                i2c_bus_1,
                i2c_bus_3,
                usb_dev,
                rtc,
                fram_cs,
                fram_spi,
            },
        )
    }

    // Optional idle, can be removed if not needed.
    #[idle]
    fn idle(_: idle::Context) -> ! {
        defmt::info!("idle");

        loop {
            continue;
        }
    }

    #[task(priority = 1, local = [fram_cs, fram_spi])]
    async fn spi_check (cx: spi_check::Context) {

        let spi_check::LocalResources {
            fram_cs,
            fram_spi,
            ..
        } = cx.local;

        fram_cs.set_low();
        fram_spi.write(&[6]);
        fram_cs.set_high();
        //Systick::delay(10.millis()).await;
        //fram_cs.set_low();
        //fram_spi.write(&[2, 0, 0, 45]).unwrap();
        //fram_cs.set_high();
        Systick::delay(10.millis()).await;
        let mut fram_read_buf = [0; 2];
        fram_cs.set_low();
        fram_spi.transfer(&mut fram_read_buf, &[5, 0]);
        fram_cs.set_high();
        defmt::info!("spi read {}", fram_read_buf);
        let mut fram_read_buf = [0;4];
        fram_cs.set_low();
        fram_spi.transfer(&mut fram_read_buf, &[3,0,0,0]);
        fram_cs.set_high();
        defmt::info!("spi read {}", fram_read_buf);

    }


    // TODO: Add tasks
    #[task(priority = 1, local = [i2c_bus_1, i2c_bus_3, rtc], shared = [usb_serial])]
    async fn i2c_read(cx: i2c_read::Context) {
        defmt::info!("Hello from the i2c task!");
        
        // move context vars to local
        let i2c_read::LocalResources {
            i2c_bus_1,
            i2c_bus_3,
            rtc,
            ..
        } = cx.local;
        let i2c_read::SharedResources {
            mut usb_serial,
            ..
        } = cx.shared;

        const PRESSURE_BUF_LEN: usize = 3;
        const TEMPERATURE_BUF_LEN: usize = 2;
        let mut pressure_buf = [0; PRESSURE_BUF_LEN];
        let mut temperature_buf = [0; TEMPERATURE_BUF_LEN];

        // delay to allow the pressure sensor to be ready
        Systick::delay(100.millis()).await;
        loop {
            match i2c_bus_1.write_read(BMP280_ADDRESS, &[0xF7,], &mut pressure_buf) {
                Ok(_) => {
                    let mut hold_buf = [0; 4];
                    hold_buf[1..4].clone_from_slice(&pressure_buf);
                    // not much point converting the pressure at the moment, as we are simply tesing the i2c port
                    // instead just give the raw value as an i20
                    let raw_pressure = i32::from_be_bytes(hold_buf) >> 4;
                    defmt::info!("raw data: {}", pressure_buf);
                    defmt::info!("pressure: {}", raw_pressure);
                    usb_serial.lock(|usb_serial| {
                        let mut write_offset = 0;
                        while write_offset < PRESSURE_BUF_LEN {
                            if let Ok(len) = usb_serial.write(&mut pressure_buf[write_offset..PRESSURE_BUF_LEN]) {
                                write_offset += len;
                            }
                        }
                    })
                }
                Err(error) => i2c_error(1, error),
            }

            match i2c_bus_3.write_read(BME280_ADDRESS, &[0x05,], &mut temperature_buf) {
                Ok(_) => {
                    let raw_temperature = (u16::from_be_bytes(temperature_buf) & 0x0FFF) as f32 / 16.0;
                    defmt::info!("raw data: {}", temperature_buf);
                    defmt::info!("temperature: {:X}", raw_temperature);
                    usb_serial.lock(|usb_serial| {
                        let mut write_offset = 0;
                        while write_offset < TEMPERATURE_BUF_LEN {
                            if let Ok(len) = usb_serial.write(&mut temperature_buf[write_offset..TEMPERATURE_BUF_LEN]) {
                                write_offset += len;
                            }
                        }
                    })
                }
                Err(error) => i2c_error(1, error),
            }

            let datetime = rtc.get_datetime();
            defmt::info!("date: {}-{}-{} {}:{}:{}", datetime.year(), datetime.month() as u8, datetime.day(), datetime.hour(), datetime.minute(), datetime.second());
            Systick::delay(5.secs()).await;
        }
    }


    #[task(binds = EXTI15_10, local = [valve1_out, valve3_out, shutdown_in, valve1_in])]
    fn shutdown_valve1_handler(cx: shutdown_valve1_handler::Context) {
        defmt::info!("shutdown interrupt");

        // move context vars to local
        let valve1_out = cx.local.valve1_out;
        let valve3_out = cx.local.valve3_out;
        let shutdown_in = cx.local.shutdown_in;
        let valve1_in = cx.local.valve1_in;

        interrupt_to_out(shutdown_in, valve1_out, "shutdown");
        interrupt_to_out(valve1_in, valve3_out, "valve 1");
    }


    #[task(binds = EXTI2, local = [valve4_out, valve3_in])]
    fn valve3_handler(cx: valve3_handler::Context) {
        defmt::info!("valve3 interrupt");

        // move context vars to local
        let valve3_handler::LocalResources {
            valve4_out,
            valve3_in,
            ..
        } = cx.local;

        interrupt_to_out(valve3_in, valve4_out, "valve 3");
    }


    #[task(binds = EXTI9_5, local = [fan_out, valve4_in])]
    fn valve4_handler(cx: valve4_handler::Context) {
        defmt::info!("valve4 interrupt");

        // move context vars to local
        let fan_out = cx.local.fan_out;
        let valve4_in = cx.local.valve4_in;

        interrupt_to_out(valve4_in, fan_out, "valve 4");
    }


    #[task(binds = OTG_FS, local = [usb_dev], shared = [usb_serial])]
    fn usb_handler (cx: usb_handler::Context) {
        let usb_handler::LocalResources {
            usb_dev,
            ..
        } = cx.local;

        let usb_handler::SharedResources {
            mut usb_serial,
            ..
        } = cx.shared;


        usb_serial.lock(|usb_serial|{
            if usb_dev.poll(&mut [usb_serial]) {
                let mut buf = [0u8; 64];

                match usb_serial.read(&mut buf) {
                    Ok(count) if count > 0 => {
                        let mut write_offset = 0;
                        while write_offset < count {
                            match usb_serial.write(&mut buf[write_offset..count]) {
                                Ok(len) if len > 0 => {
                                    write_offset += len;
                                }
                                _ => {}
                            }
                        }
                    }
                    _ => {}
                }
            }
        });
    }


    fn setup_interrupt_pin<const P: char, const N: u8>(pin: Pin<P, N>, syscfg: &mut SysCfg, exti: &mut EXTI) -> Pin<P, N> {
        let mut interrupt_pin = pin.into_input();
        interrupt_pin.make_interrupt_source(syscfg);
        interrupt_pin.trigger_on_edge(exti, Edge::RisingFalling);
        interrupt_pin.enable_interrupt(exti);

        interrupt_pin
    }


    fn interrupt_to_out<const PI: char, const NI: u8, const PO: char, const NO: u8>(in_pin: &mut Pin<PI, NI>, out_pin: &mut Pin<PO, NO, Output>, pin_description: &str) {
        if in_pin.check_interrupt() {
            defmt::info!("interrupt on {} pin", pin_description);
            in_pin.clear_interrupt_pending_bit();
            out_pin.set_state(in_pin.is_high().into());
        }

    }


    fn i2c_error (bus_id: u8, error: i2c::Error) {
        match error {
            i2c::Error::Overrun => defmt::warn!("i2c{} overrun", bus_id),
            i2c::Error::NoAcknowledge(_source) => defmt::warn!("i2c{} no acknowledge", bus_id),
            i2c::Error::Timeout => defmt::warn!("i2c{} timeout", bus_id),
            i2c::Error::Bus => defmt::warn!("i2c{} buss error", bus_id),
            i2c::Error::ArbitrationLoss => defmt::warn!("i2c{} arbitration lost", bus_id),
            _ => defmt::warn!("i2c{} unknown error", bus_id),
        }
    }
}
